package dz.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author Iskander Valiev
 * created by isko
 * on 6/6/19
 */
@Data
@Builder
public class VisitStatisticForTodayDto {

    private Long uniqueVisitsCountForToday;

    private Long visitsCountForToday;
}
