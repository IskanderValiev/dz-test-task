package dz.repositories;

import dz.models.Visit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;

public interface VisitRepository extends JpaRepository<Visit, Long> {
    Long countAllByVisitDateBetween(LocalDateTime from, LocalDateTime to);

    @Query("select count (distinct v.userId) from Visit v where v.visitDate between :from and :to")
    Long countUniqueVisits(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to);

    @Query(value = "select count(*) from (select case when count(distinct (user_id, page_id)) >= 10 then (user_id)\n" +
            "                                else null end from visit where visit_date between :from and :to group by user_id) as v where v notnull", nativeQuery = true)
    Long countRegularVisits(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to);
}
