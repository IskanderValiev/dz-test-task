package dz.controllers;

import dz.dto.VisitStatisticDto;
import dz.dto.VisitStatisticForTodayDto;
import dz.services.VisitService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

/**
 * @author Iskander Valiev
 * created by isko
 * on 6/5/19
 */
@RestController
@Api(tags = "Visit Statistic Controller")
@RequestMapping("/visits")
public class VisitStatisticController {

    @Autowired
    private VisitService visitService;

    @GetMapping
    public ResponseEntity<VisitStatisticDto> getStatistic(@RequestParam("from")
                                                           @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                                           @RequestParam("to")
                                                           @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        return ResponseEntity.ok(visitService.getStatistic(from, to));
    }

    @PostMapping("/create")
    public ResponseEntity<VisitStatisticForTodayDto> createVisit(@RequestParam Long userId, @RequestParam Long pageId) {
        VisitStatisticForTodayDto statistic = visitService.registerVisit(userId, pageId);
        return ResponseEntity.ok(statistic);
    }
}
