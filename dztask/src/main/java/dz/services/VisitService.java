package dz.services;

import dz.dto.VisitStatisticDto;
import dz.dto.VisitStatisticForTodayDto;

import java.time.LocalDate;

public interface VisitService {
    VisitStatisticForTodayDto registerVisit(Long userId, Long pageId);
    VisitStatisticDto getStatistic(LocalDate from, LocalDate to);
}
