package dz.services;

import dz.dto.VisitStatisticDto;
import dz.dto.VisitStatisticForTodayDto;
import dz.models.Visit;
import dz.repositories.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * @author Iskander Valiev
 * created by isko
 * on 6/5/19
 */
@Service
public class VisitServiceImpl implements VisitService {

    @Autowired
    private VisitRepository visitRepository;

    @Override
    public VisitStatisticForTodayDto registerVisit(Long userId, Long pageId) {
        Visit visit = Visit.builder()
                .userId(userId)
                .visitDate(LocalDateTime.now())
                .pageId(pageId)
                .build();
        visitRepository.save(visit);

        LocalDate today = LocalDate.now();
        return VisitStatisticForTodayDto.builder()
                .uniqueVisitsCountForToday(getUniqueVisitsCount(today, today))
                .visitsCountForToday(getVisitsCount(today, today))
                .build();
    }

    @Override
    public VisitStatisticDto getStatistic(LocalDate from, LocalDate to) {
        return VisitStatisticDto.builder()
                .visitsCount(getVisitsCount(from, to))
                .regularVisitsCount(getRegularVisitsCount(from, to))
                .uniqueVisitsCount(getUniqueVisitsCount(from, to))
                .build();
    }

    private Long getVisitsCount(LocalDate from, LocalDate to) {
        LocalDateTime fromDateTime = from.atStartOfDay();
        LocalDateTime toDateTime = to.atTime(LocalTime.MAX);
        return visitRepository.countAllByVisitDateBetween(fromDateTime, toDateTime);
    }

    private Long getUniqueVisitsCount(LocalDate from, LocalDate to) {
        LocalDateTime fromDateTime = from.atStartOfDay();
        LocalDateTime toDateTime = to.atTime(LocalTime.MAX);
        return visitRepository.countUniqueVisits(fromDateTime, toDateTime);
    }

    private Long getRegularVisitsCount(LocalDate from, LocalDate to) {
        LocalDateTime fromDateTime = from.atStartOfDay();
        LocalDateTime toDateTime = to.atTime(LocalTime.MAX);
        return visitRepository.countRegularVisits(fromDateTime, toDateTime);
    }


}
