package dz.application;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Iskander Valiev
 * created by isko
 * on 6/5/19
 */
@SpringBootApplication
@ComponentScan("dz")
@EntityScan(basePackages = {"dz.models"})
@EnableJpaRepositories(basePackages = {"dz.repositories"})
@Slf4j
@EnableAspectJAutoProxy
public class Application {

    public static void main(String[] args) {
        log.info("Starting...");
        SpringApplication.run(Application.class, args);
    }
}
